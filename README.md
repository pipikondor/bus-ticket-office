This project have three containers bus_ticket_office (here), payment_system (https://gitlab.com/pipikondor/payment-system) and database (PostgreSql)
Database structure you can see in /docker-compose/init.sql
init.sql will run only one time when application start's.

By default you can run app with docker-compose use 

$ sudo docker-compose up --build

In /docker-compose you can get Postman collections with API request's or you can use next data.

payment-system default URL

http://localhost:8080/

Create payment
Method: POST

URL /api/payments/
Body: 
{
    "paymentSum":integer,
    "name":Name is mandatory",
    "surname":Surname is mandatory",
    "middleName":Middle is mandatory"
}

Get payment status
Method: GET

URL /api/payments/{payment-uuid}/status

******************************************************

bus_tickets_office default URL
http://localhost:9000/

Buy ticket:
Method: POST

URL /api/tickets/
Body: 

{
    "tripId":integer,
    "name":Name is mandatory",
    "surname":Surname is mandatory",
    "middleName":Middle is mandatory"
}

Check tickets:
Method: GET

URL: /api/tickets/{int ticketId}

Get all trips:
Method: GET

URL: /api/trips/


