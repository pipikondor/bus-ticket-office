CREATE TABLE passengers
(
    id          serial,
    name        varchar(50) NOT NULL,
    surname     varchar(50) NOT NULL,
    middle_name varchar(50),
    PRIMARY KEY (id)
);

CREATE TABLE trips
(
    id                  serial,
    departure_place     VARCHAR(50) NOT NULL,
    arrival_place       VARCHAR(50) NOT NULL,
    departure_date_time timestamp without time zone NOT NULL,
    price               int         NOT NULL,
    places_count        int         NOT NULL CHECK (price > 0),
    PRIMARY KEY (id)
);

CREATE TABLE payments
(
    uuid   varchar(50),
    status varchar(6) NOT NULL,
    PRIMARY KEY (uuid)
);

CREATE TABLE tickets
(
    id           serial,
    trip_id      int NOT NULL,
    passenger_id int NOT NULL,
    payment_uuid varchar(50),
    PRIMARY KEY (id),
    FOREIGN KEY (trip_id) REFERENCES trips (id),
    FOREIGN KEY (passenger_id) REFERENCES passengers (id),
    FOREIGN KEY (payment_uuid) REFERENCES payments (uuid)
);

INSERT INTO public.trips (departure_place, arrival_place, departure_date_time, price, places_count)
VALUES ('London', 'Paris', '2022-01-20 01:15:00.000', 54000, 50),
       ('Tokyo', 'Moscow', '2022-01-21 11:00:00.000', 14650, 12),
       ('London', 'Paris', '2022-01-24 21:42:00.000', 60000, 18),
       ('New York', 'Utah', '2022-02-20 18:30:00.000', 11800, 32);