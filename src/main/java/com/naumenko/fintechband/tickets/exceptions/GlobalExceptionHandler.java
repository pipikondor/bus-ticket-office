package com.naumenko.fintechband.tickets.exceptions;

import com.naumenko.fintechband.tickets.exceptions.web.ErrorInfo;
import com.naumenko.fintechband.tickets.exceptions.web.ValidationError;
import com.naumenko.fintechband.tickets.exceptions.web.ValidationInfoResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler({TicketIsNotExistsException.class, EntityNotFoundException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorInfo clientErrorHandler(Exception exception) {
        logger.warn("Client exception ", exception);
        ErrorInfo error = new ErrorInfo();
        error.setInfo(exception.getMessage());
        return error;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationInfoResponse validationExceptionHandler(ConstraintViolationException exception) {
        logger.error("Client validation exception ", exception);
        ValidationInfoResponse error = new ValidationInfoResponse();
        for (ConstraintViolation violation : exception.getConstraintViolations()) {
            error.getErrors().add(
                new ValidationError(violation.getPropertyPath().toString(), violation.getMessage()));
        }
        return error;
    }

    @ExceptionHandler({UnsupportedExternalResponseException.class, IllegalArgumentException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorInfo serverErrorHandler(Exception exception) {
        logger.warn("Server exception ", exception);
        ErrorInfo error = new ErrorInfo();
        error.setInfo("Something wen wrong. Please try again letter");
        return error;
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorInfo handleMessageNotReadableException(Exception exception) {
        ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setInfo(exception.getMessage());
        return errorInfo;
    }
}
