package com.naumenko.fintechband.tickets.exceptions;

public class TicketIsNotExistsException extends RuntimeException {

    public TicketIsNotExistsException(String message) {
        super(message);
    }
}
