package com.naumenko.fintechband.tickets.exceptions.web;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ValidationInfoResponse {

    private List<ValidationError> errors;

    public ValidationInfoResponse() {
        this.errors = new ArrayList<>();
    }
}
