package com.naumenko.fintechband.tickets.exceptions.web;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorInfo {

    private String info;
}
