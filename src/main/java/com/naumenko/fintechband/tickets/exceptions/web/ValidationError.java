package com.naumenko.fintechband.tickets.exceptions.web;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ValidationError {

    private String fieldName;
    private String message;
}
