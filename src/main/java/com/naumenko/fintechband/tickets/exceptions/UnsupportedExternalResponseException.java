package com.naumenko.fintechband.tickets.exceptions;

public class UnsupportedExternalResponseException extends RuntimeException {

    public UnsupportedExternalResponseException(String message) {
        super(message);
    }
}
