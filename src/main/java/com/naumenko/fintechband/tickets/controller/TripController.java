package com.naumenko.fintechband.tickets.controller;

import com.naumenko.fintechband.tickets.model.TripViewDto;
import com.naumenko.fintechband.tickets.service.interfaces.TripService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/trips")
public class TripController {

    private static final Logger logger = LoggerFactory.getLogger(TripController.class);
    private final TripService tripService;

    @Autowired
    public TripController(TripService tripService) {
        this.tripService = tripService;
    }

    @GetMapping
    public List<TripViewDto> findAllTrips() {
        logger.info("GET /api/trips");
        return tripService.findAllWithAvailablePlaces();
    }
}
