package com.naumenko.fintechband.tickets.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.naumenko.fintechband.tickets.model.TicketOrderDto;
import com.naumenko.fintechband.tickets.entity.Ticket;
import com.naumenko.fintechband.tickets.model.Views;
import com.naumenko.fintechband.tickets.service.interfaces.TicketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tickets")
public class TicketController {

    private static final Logger logger = LoggerFactory.getLogger(TicketController.class);
    private final TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @GetMapping("/{id}")
    @JsonView(Views.TicketInfo.class)
    public Ticket getTicketById(@PathVariable int id) {
        logger.info("GET /api/tickets/{}", id);
        return ticketService.findById(id);
    }

    @PostMapping
    @JsonView(Views.Id.class)
    public Ticket buyTicket(@RequestBody TicketOrderDto ticket) {
        logger.info("POST /api/tickets with tripId {}", ticket);
        return ticketService.buyTicket(ticket);
    }
}
