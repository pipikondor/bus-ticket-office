package com.naumenko.fintechband.tickets.model;

import com.naumenko.fintechband.tickets.model.interfaces.Dto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
public class PaymentResponseDto implements Dto {

    @NotEmpty(message = "UUID should not be null or empty")
    private String uuid;
}
