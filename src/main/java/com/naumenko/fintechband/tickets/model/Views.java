package com.naumenko.fintechband.tickets.model;

public final class Views {

    public interface Id {}

    public interface TripInfo {}

    public interface PaymentInfo {}

    public interface TicketInfo extends TripInfo, PaymentInfo{}
}
