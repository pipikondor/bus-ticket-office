package com.naumenko.fintechband.tickets.model;

import com.naumenko.fintechband.tickets.model.interfaces.Dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentRequestDto implements Dto {

    @Min(value = 1, message = "Incorrect ticket price")
    private int paymentSum;
    @NotEmpty(message = "Name is mandatory")
    private String name;
    @NotEmpty(message = "Surname is mandatory")
    private String surname;
    @NotEmpty(message = "MiddleName is mandatory")
    private String middleName;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PaymentRequestDto that = (PaymentRequestDto) o;
        return paymentSum == that.paymentSum && Objects.equals(name, that.name) &&
            Objects.equals(surname, that.surname) && Objects.equals(middleName, that.middleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(paymentSum, name, surname, middleName);
    }
}

