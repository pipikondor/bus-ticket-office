package com.naumenko.fintechband.tickets.model;

import com.naumenko.fintechband.tickets.model.interfaces.Dto;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PaymentCheckResponseDto implements Dto {

    @NotNull
    private PaymentStatus paymentStatus;
}
