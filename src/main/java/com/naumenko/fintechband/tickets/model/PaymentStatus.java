package com.naumenko.fintechband.tickets.model;

public enum PaymentStatus {

    NEW, FAILED, DONE
}
