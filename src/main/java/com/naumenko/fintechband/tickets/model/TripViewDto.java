package com.naumenko.fintechband.tickets.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Calendar;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
public class TripViewDto {

    private int id;
    private String departurePlace;
    private String arrivalPlace;
    private Calendar departureDateTime;
    private int price;
    private int availablePlacesCount;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TripViewDto that = (TripViewDto) o;
        return price == that.price && availablePlacesCount == that.availablePlacesCount &&
            Objects.equals(departurePlace, that.departurePlace) &&
            Objects.equals(arrivalPlace, that.arrivalPlace) &&
            Objects.equals(departureDateTime, that.departureDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departurePlace, arrivalPlace, departureDateTime, price, availablePlacesCount);
    }
}
