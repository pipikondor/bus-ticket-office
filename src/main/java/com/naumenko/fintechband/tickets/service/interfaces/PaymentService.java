package com.naumenko.fintechband.tickets.service.interfaces;

import com.naumenko.fintechband.tickets.entity.Payment;

import java.util.List;

public interface PaymentService {

    List<Payment> findAll();

    Payment updateStatus(Payment payment);
}
