package com.naumenko.fintechband.tickets.service;

import com.naumenko.fintechband.tickets.model.PaymentCheckResponseDto;
import com.naumenko.fintechband.tickets.model.PaymentRequestDto;
import com.naumenko.fintechband.tickets.model.PaymentResponseDto;
import com.naumenko.fintechband.tickets.model.interfaces.Dto;
import com.naumenko.fintechband.tickets.entity.Passenger;
import com.naumenko.fintechband.tickets.model.PaymentStatus;
import com.naumenko.fintechband.tickets.exceptions.UnsupportedExternalResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;

@Component
public class PaymentRestClient {

    private static final Logger logger = LoggerFactory.getLogger(PaymentRestClient.class);
    private final RestTemplate restTemplate;
    private final Validator validator;

    @Value("${payment.url}")
    private String baseUrl;

    @Autowired
    public PaymentRestClient(RestTemplate restTemplate, Validator validator) {
        this.restTemplate = restTemplate;
        this.validator = validator;
    }

    public String createPayment(Passenger passenger, int paymentSum) {
        if (passenger == null) {
            throw new IllegalArgumentException("Passenger should not be null");
        }
        PaymentRequestDto request = new PaymentRequestDto(paymentSum, passenger.getName(),
            passenger.getSurname(), passenger.getMiddleName());
        validateDto(request);
        ResponseEntity<PaymentResponseDto> responseEntity =
            restTemplate.postForEntity(baseUrl, request, PaymentResponseDto.class);
        PaymentResponseDto paymentResponse = responseEntity.getBody();
        try {
            validateDto(paymentResponse);
        } catch (ConstraintViolationException | IllegalArgumentException exc) {
            throw new UnsupportedExternalResponseException("External response is not valid or empty");
        }
        logger.info("Payment create successful, payment uuid is {}", paymentResponse.getUuid());
        return paymentResponse.getUuid();
    }

    public PaymentStatus checkPaymentStatus(String paymentUUID) {
        if (paymentUUID == null) {
            throw new IllegalArgumentException("Payment UUID should not be null");
        }
        String checkStatusURL = baseUrl + paymentUUID + "/status";
        ResponseEntity<PaymentCheckResponseDto> response = restTemplate.exchange(checkStatusURL, HttpMethod.GET, null,
            new ParameterizedTypeReference<PaymentCheckResponseDto>() {});
        PaymentCheckResponseDto checkResponse = response.getBody();
        try {
            validateDto(checkResponse);
        } catch (ConstraintViolationException | IllegalArgumentException exc) {
            throw new UnsupportedExternalResponseException("External response is not valid or empty");
        }
        logger.info("payment {} has status {}", paymentUUID, checkResponse.getPaymentStatus());
        return checkResponse.getPaymentStatus();
    }

    private void validateDto(Dto input) {
        Set<ConstraintViolation<Dto>> violations = validator.validate(input);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}
