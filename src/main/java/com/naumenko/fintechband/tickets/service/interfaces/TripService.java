package com.naumenko.fintechband.tickets.service.interfaces;

import com.naumenko.fintechband.tickets.model.TripViewDto;

import java.util.List;

public interface TripService {

    List<TripViewDto> findAllWithAvailablePlaces();
}
