package com.naumenko.fintechband.tickets.service;

import com.naumenko.fintechband.tickets.model.TicketOrderDto;
import com.naumenko.fintechband.tickets.entity.Passenger;
import com.naumenko.fintechband.tickets.entity.Payment;
import com.naumenko.fintechband.tickets.model.PaymentStatus;
import com.naumenko.fintechband.tickets.entity.Ticket;
import com.naumenko.fintechband.tickets.entity.Trip;
import com.naumenko.fintechband.tickets.exceptions.TicketIsNotExistsException;
import com.naumenko.fintechband.tickets.repository.TicketRepository;
import com.naumenko.fintechband.tickets.repository.TripRepository;
import com.naumenko.fintechband.tickets.service.interfaces.TicketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class TicketServiceImpl implements TicketService {

    private static final  Logger logger = LoggerFactory.getLogger(TicketServiceImpl.class);
    private final TicketRepository ticketRepository;
    private final TripRepository tripRepository;
    private final PaymentRestClient paymentRestClient;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository, TripRepository tripRepository, PaymentRestClient paymentRestClient) {
        this.ticketRepository = ticketRepository;
        this.tripRepository = tripRepository;
        this.paymentRestClient = paymentRestClient;
    }

    @Override
    public Ticket findById(int id) {
        return ticketRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Incorrect ticket id"));
    }

    @Override
    public synchronized Ticket buyTicket(TicketOrderDto order) {
        Trip trip = tripRepository.findById(order.getTripId())
            .orElseThrow(() -> new EntityNotFoundException("Incorrect trip id"));
        if (!checkTicketAvailability(trip)) {
            throw new TicketIsNotExistsException("No tickets for the trip");
        }
        Passenger passenger = new Passenger(order.getName(), order.getSurname(), order.getMiddleName());
        Payment payment = new Payment(paymentRestClient.createPayment(passenger, trip.getPrice()), PaymentStatus.NEW);
        Ticket ticket = new Ticket(trip, passenger, payment);
        ticket = ticketRepository.save(ticket);
        logger.info("ticket {} to trip {} was saved", ticket.getTicketId(), order.getTripId());
        return ticket;
    }

    private boolean checkTicketAvailability(Trip trip) {
        int soldTicketCount = ticketRepository.getTicketCount(trip.getId());
        int totalTicketsCount = trip.getTotalPlacesCount();
        if (soldTicketCount < totalTicketsCount) {
            logger.info("There are {} free tickets on the trip with id {}", totalTicketsCount - soldTicketCount, trip.getId());
            return true;
        }
        logger.info("There are no tickets on the trip with id {}", trip.getId());
        return false;
    }
}
