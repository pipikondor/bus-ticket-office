package com.naumenko.fintechband.tickets.service.interfaces;

import com.naumenko.fintechband.tickets.model.TicketOrderDto;
import com.naumenko.fintechband.tickets.entity.Ticket;

public interface TicketService {

    Ticket findById(int id);

    Ticket buyTicket(TicketOrderDto ticket);
}
