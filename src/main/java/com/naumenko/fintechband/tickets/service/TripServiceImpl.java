package com.naumenko.fintechband.tickets.service;

import com.naumenko.fintechband.tickets.model.TripViewDto;
import com.naumenko.fintechband.tickets.entity.Trip;
import com.naumenko.fintechband.tickets.repository.TicketRepository;
import com.naumenko.fintechband.tickets.repository.TripRepository;
import com.naumenko.fintechband.tickets.service.interfaces.TripService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TripServiceImpl implements TripService {

    private static final Logger logger = LoggerFactory.getLogger(TripServiceImpl.class);
    private final TripRepository tripRepository;
    private final TicketRepository ticketRepository;

    @Autowired
    public TripServiceImpl(TripRepository tripRepository, TicketRepository ticketRepository) {
        this.tripRepository = tripRepository;
        this.ticketRepository = ticketRepository;
    }

    @Override
    public List<TripViewDto> findAllWithAvailablePlaces() {
        List<Trip> trips = tripRepository.findAll();
        List<TripViewDto> tripsDto = trips.stream().map(trip -> {
            int soldTicketCount = ticketRepository.getTicketCount(trip.getId());
            int availableTicketsCount = trip.getTotalPlacesCount() - soldTicketCount;
            return new TripViewDto(
                trip.getId(),
                trip.getDeparturePlace(),
                trip.getArrivalPlace(),
                trip.getDepartureDateTime(),
                trip.getPrice(),
                availableTicketsCount
            );
        }).collect(Collectors.toList());
        logger.info("findAll return {} rows", tripsDto.size());
        return tripsDto;
    }
}
