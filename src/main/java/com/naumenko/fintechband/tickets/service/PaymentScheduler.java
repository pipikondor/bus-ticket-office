package com.naumenko.fintechband.tickets.service;

import com.naumenko.fintechband.tickets.entity.Payment;
import com.naumenko.fintechband.tickets.service.interfaces.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PaymentScheduler {

    private static final Logger logger = LoggerFactory.getLogger(PaymentScheduler.class);
    private final PaymentService paymentService;

    @Autowired
    public PaymentScheduler(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Scheduled(fixedRate = 1000 * 60)
    public void checkPaymentsStatus() {
        logger.info("schedule checking NEW payments");
        List<Payment> payments = paymentService.findAll();
        for (Payment payment: payments) {
            try {
                paymentService.updateStatus(payment);
            } catch (Exception exception) {
                logger.error("Scheduler got exception", exception);
            }
        }
    }
}
