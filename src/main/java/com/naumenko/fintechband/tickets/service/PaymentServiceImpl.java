package com.naumenko.fintechband.tickets.service;

import com.naumenko.fintechband.tickets.entity.Payment;
import com.naumenko.fintechband.tickets.model.PaymentStatus;
import com.naumenko.fintechband.tickets.entity.Ticket;
import com.naumenko.fintechband.tickets.entity.Trip;
import com.naumenko.fintechband.tickets.repository.PaymentRepository;
import com.naumenko.fintechband.tickets.repository.TicketRepository;
import com.naumenko.fintechband.tickets.repository.TripRepository;
import com.naumenko.fintechband.tickets.service.interfaces.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    private static final Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);
    private final PaymentRepository paymentRepository;
    private final TicketRepository ticketRepository;
    private final TripRepository tripRepository;
    private final PaymentRestClient paymentRestClient;

    @Autowired
    public PaymentServiceImpl(PaymentRepository paymentRepository, TicketRepository ticketRepository, TripRepository tripRepository, PaymentRestClient paymentRestClient) {
        this.paymentRepository = paymentRepository;
        this.ticketRepository = ticketRepository;
        this.tripRepository = tripRepository;
        this.paymentRestClient = paymentRestClient;
    }

    public List<Payment> findAll() {
        List<Payment> payments = paymentRepository.findAll();
        logger.info("find all return {} row(s)", payments.size());
        return payments;
    }

    public Payment updateStatus(Payment payment) {
        PaymentStatus status = paymentRestClient.checkPaymentStatus(payment.getUuid());
        if (status == PaymentStatus.NEW) {
            return payment;
        }
        if (status == PaymentStatus.FAILED) {
            Ticket ticket = ticketRepository.findTicketByPayment(payment);
            if (ticket == null) {
                throw new EntityNotFoundException(String.format("Ticket with payment %s is not found", payment.getUuid()));
            }
            Trip trip = ticket.getTrip();
            trip.setTotalPlacesCount(trip.getTotalPlacesCount() + 1);
            tripRepository.save(trip);
            logger.info("add 1 place to trip with id {}", trip.getId());
        }
        payment.setPaymentStatus(status);
        logger.info("save payment with uid {} {}", payment.getUuid(), payment.getPaymentStatus());
        return paymentRepository.save(payment);
    }
}
