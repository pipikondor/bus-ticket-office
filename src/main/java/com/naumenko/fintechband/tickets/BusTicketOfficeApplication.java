package com.naumenko.fintechband.tickets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@PropertySource("classpath:web_client.properties")
@EnableScheduling
public class BusTicketOfficeApplication {

    public static void main(String[] args) {
        SpringApplication.run(BusTicketOfficeApplication.class, args);
    }
}
