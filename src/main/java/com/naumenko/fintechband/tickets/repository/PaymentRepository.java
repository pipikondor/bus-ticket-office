package com.naumenko.fintechband.tickets.repository;

import com.naumenko.fintechband.tickets.entity.Payment;
import com.naumenko.fintechband.tickets.model.PaymentStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {

    List<Payment> findAllByPaymentStatus(PaymentStatus paymentStatus);
}
