package com.naumenko.fintechband.tickets.repository;

import com.naumenko.fintechband.tickets.entity.Trip;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TripRepository extends JpaRepository<Trip, Integer> {
}
