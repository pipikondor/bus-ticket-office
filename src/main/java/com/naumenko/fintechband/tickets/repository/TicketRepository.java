package com.naumenko.fintechband.tickets.repository;

import com.naumenko.fintechband.tickets.entity.Payment;
import com.naumenko.fintechband.tickets.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {

    Ticket findTicketByPayment(Payment payment);

    @Query("select count(t) from Ticket t where t.trip.id = ?1 and not t.payment.paymentStatus = 'FAILED'")
    int getTicketCount(int tripId);
}
