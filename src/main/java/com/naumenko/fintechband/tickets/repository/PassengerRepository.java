package com.naumenko.fintechband.tickets.repository;

import com.naumenko.fintechband.tickets.entity.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassengerRepository extends JpaRepository<Passenger, Integer> {
}
