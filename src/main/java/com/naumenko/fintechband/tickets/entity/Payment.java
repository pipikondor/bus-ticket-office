package com.naumenko.fintechband.tickets.entity;

import com.fasterxml.jackson.annotation.JsonView;
import com.naumenko.fintechband.tickets.model.PaymentStatus;
import com.naumenko.fintechband.tickets.model.Views;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "payments")
@NoArgsConstructor
@AllArgsConstructor
public class Payment {

    @Id
    @Column(name = "uuid")
    private String uuid;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    @JsonView(Views.PaymentInfo.class)
    private PaymentStatus paymentStatus;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Payment payment = (Payment) o;
        return Objects.equals(uuid, payment.uuid) && paymentStatus == payment.paymentStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, paymentStatus);
    }
}
