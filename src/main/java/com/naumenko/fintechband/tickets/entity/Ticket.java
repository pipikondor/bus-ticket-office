package com.naumenko.fintechband.tickets.entity;

import com.fasterxml.jackson.annotation.JsonView;
import com.naumenko.fintechband.tickets.model.Views;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tickets")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonView(Views.Id.class)
    private int ticketId;

    @ManyToOne
    @JoinColumn(name = "trip_id")
    @JsonView(Views.TripInfo.class)
    private Trip trip;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "passenger_id")
    private Passenger passenger;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "payment_uuid")
    @JsonView(Views.PaymentInfo.class)
    private Payment payment;

    public Ticket(Trip trip, Passenger passenger, Payment payment) {
        this.trip = trip;
        this.passenger = passenger;
        this.payment = payment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ticket ticket = (Ticket) o;
        return Objects.equals(trip, ticket.trip) && Objects.equals(passenger, ticket.passenger) &&
            Objects.equals(payment, ticket.payment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(passenger, payment);
    }
}
