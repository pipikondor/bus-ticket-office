package com.naumenko.fintechband.tickets.entity;

import com.fasterxml.jackson.annotation.JsonView;
import com.naumenko.fintechband.tickets.model.Views;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Calendar;
import java.util.Objects;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "trips")
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "departure_place")
    @JsonView(Views.TripInfo.class)
    private String departurePlace;

    @Column(name = "arrival_place")
    @JsonView(Views.TripInfo.class)
    private String arrivalPlace;

    @Column(name = "departure_date_time")
    @JsonView(Views.TripInfo.class)
    private Calendar departureDateTime;

    @Column(name = "price")
    private int price;

    @Column(name = "places_count")
    private int totalPlacesCount;

    public Trip(String departurePlace, String arrivalPlace, Calendar departureDateTime, int price, int totalPlacesCount) {
        this.departurePlace = departurePlace;
        this.arrivalPlace = arrivalPlace;
        this.departureDateTime = departureDateTime;
        this.price = price;
        this.totalPlacesCount = totalPlacesCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Trip trip = (Trip) o;
        return price == trip.price && totalPlacesCount == trip.totalPlacesCount &&
            Objects.equals(departurePlace, trip.departurePlace) &&
            Objects.equals(arrivalPlace, trip.arrivalPlace) &&
            Objects.equals(departureDateTime, trip.departureDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departurePlace, arrivalPlace, departureDateTime, price, totalPlacesCount);
    }
}
