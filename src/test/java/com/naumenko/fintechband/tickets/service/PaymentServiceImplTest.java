package com.naumenko.fintechband.tickets.service;

import com.naumenko.fintechband.tickets.entity.Passenger;
import com.naumenko.fintechband.tickets.entity.Payment;
import com.naumenko.fintechband.tickets.model.PaymentStatus;
import com.naumenko.fintechband.tickets.entity.Ticket;
import com.naumenko.fintechband.tickets.entity.Trip;
import com.naumenko.fintechband.tickets.repository.PaymentRepository;
import com.naumenko.fintechband.tickets.repository.TicketRepository;
import com.naumenko.fintechband.tickets.repository.TripRepository;
import com.naumenko.fintechband.tickets.service.interfaces.PaymentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityNotFoundException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestPropertySource("classpath:app.test.properties")
class PaymentServiceImplTest {

    @Autowired
    private PaymentService paymentService;

    @MockBean
    private PaymentRestClient paymentRestClient;

    @MockBean
    private PaymentRepository paymentRepository;

    @MockBean
    private TripRepository tripRepository;

    @MockBean
    private TicketRepository ticketRepository;

    @Test
    void updateStatus_shouldReturnInputPayment_whenPaymentStatusIsNew() {
        Payment inputPayment = new Payment("123qwe", PaymentStatus.NEW);
        Payment exceptedPayment = new Payment("123qwe", PaymentStatus.NEW);
        Mockito.when(paymentRestClient.checkPaymentStatus("123qwe")).thenReturn(PaymentStatus.NEW);
        Assertions.assertEquals(exceptedPayment, paymentService.updateStatus(inputPayment));
        Mockito.verify(tripRepository, Mockito.times(0)).save(any(Trip.class));
        Mockito.verify(paymentRepository, Mockito.times(0)).save(any(Payment.class));
    }

    @Test
    void updateStatus_shouldReturnChangedPaymentAndChangeTrip_whenPaymentStatusIsFailed() {
        Calendar date = new GregorianCalendar(2020, Calendar.FEBRUARY, 5);
        date.set(Calendar.HOUR, 10);
        date.set(Calendar.MINUTE, 12);
        Trip trip = new Trip("London", "New York", date, 12, 20);
        Passenger passenger = new Passenger("qwe", "asd", "tyu");
        Payment inputPayment = new Payment("123qwe", PaymentStatus.NEW);
        passenger.setId(1);
        trip.setId(112);
        Ticket ticket = new Ticket(trip, passenger, inputPayment);
        ticket.setTicketId(19);
        Payment exceptedPayment = new Payment("123qwe", PaymentStatus.FAILED);
        Mockito.when(paymentRestClient.checkPaymentStatus("123qwe")).thenReturn(PaymentStatus.FAILED);
        Mockito.when(ticketRepository.findTicketByPayment(inputPayment)).thenReturn(ticket);
        Mockito.when(paymentRepository.save(inputPayment)).thenReturn(inputPayment);
        Assertions.assertEquals(exceptedPayment, paymentService.updateStatus(inputPayment));
        Mockito.verify(tripRepository, Mockito.times(1)).save(any(Trip.class));
        Mockito.verify(paymentRepository, Mockito.times(1)).save(any(Payment.class));
    }

    @Test
    void updateStatus_shouldThrowEntityNotFoundException_whenPaymentStatusIsFailedAndTicketIsNotFound() {
        Payment inputPayment = new Payment("123qwe", PaymentStatus.NEW);
        Mockito.when(paymentRestClient.checkPaymentStatus("123qwe")).thenReturn(PaymentStatus.FAILED);
        Assertions.assertThrows(EntityNotFoundException.class, () -> paymentService.updateStatus(inputPayment));
        Mockito.verify(tripRepository, Mockito.times(0)).save(any(Trip.class));
        Mockito.verify(paymentRepository, Mockito.times(0)).save(any(Payment.class));
    }

    @Test
    void updateStatus_shouldReturnChangedPaymentAndShouldNotChangeTrip_whenPaymentStatusIsDone() {
        Passenger passenger = new Passenger("qwe", "asd", "tyu");
        Payment inputPayment = new Payment("123qwe", PaymentStatus.NEW);
        Payment exceptedPayment = new Payment("123qwe", PaymentStatus.DONE);
        Mockito.when(paymentRestClient.checkPaymentStatus("123qwe")).thenReturn(PaymentStatus.DONE);
        Mockito.when(paymentRepository.save(inputPayment)).thenReturn(inputPayment);
        Assertions.assertEquals(exceptedPayment, paymentService.updateStatus(inputPayment));
        Mockito.verify(tripRepository, Mockito.times(0)).save(any(Trip.class));
        Mockito.verify(paymentRepository, Mockito.times(1)).save(any(Payment.class));
    }
}
