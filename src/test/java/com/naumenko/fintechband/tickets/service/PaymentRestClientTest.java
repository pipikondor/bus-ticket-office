package com.naumenko.fintechband.tickets.service;

import com.naumenko.fintechband.tickets.model.PaymentCheckResponseDto;
import com.naumenko.fintechband.tickets.model.PaymentRequestDto;
import com.naumenko.fintechband.tickets.model.PaymentResponseDto;
import com.naumenko.fintechband.tickets.entity.Passenger;
import com.naumenko.fintechband.tickets.model.PaymentStatus;
import com.naumenko.fintechband.tickets.exceptions.UnsupportedExternalResponseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.validation.ConstraintViolationException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(SpringExtension.class)
@TestPropertySource("classpath:app.test.properties")
@SpringBootTest
class PaymentRestClientTest {

    @Value("${payment.url}")
    private String testURL;

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private PaymentRestClient paymentRestClient;

    @Test
    void createPayment_shouldThrowIllegalArgumentException_whenPassengerIsNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            paymentRestClient.createPayment(null, 10));
        Mockito.verify(restTemplate, Mockito.times(0))
            .postForEntity(anyString(), any(), any());
    }

    @Test
    void createPayment_shouldThrowHttpClientErrorException_whenAPIReturnClientException() {
        Passenger passenger = new Passenger("qwe", "asd", "zxc");
        PaymentRequestDto request = new PaymentRequestDto(100, "qwe", "asd", "zxc");
        Mockito.when(restTemplate.postForEntity(testURL, request, PaymentResponseDto.class))
            .thenThrow(HttpClientErrorException.class);
        Assertions.assertThrows(HttpClientErrorException.class, () ->
            paymentRestClient.createPayment(passenger, 100));
        Mockito.verify(restTemplate, Mockito.times(1)).
            postForEntity(anyString(), any(PaymentRequestDto.class), any());
    }

    @Test
    void createPayment_shouldThrowUnsupportedExternalResponseException_whenPaymentResponseBodyIsNull() {
        Passenger passenger = new Passenger("qwe", "asd", "zxc");
        Mockito.when(restTemplate.postForEntity(any(String.class), any(PaymentRequestDto.class), any()))
            .thenReturn(new ResponseEntity(HttpStatus.OK));
        Assertions.assertThrows(UnsupportedExternalResponseException.class, () ->
            paymentRestClient.createPayment(passenger, 100));
        Mockito.verify(restTemplate, Mockito.times(1)).
            postForEntity(anyString(), any(PaymentRequestDto.class), any());
    }

    @Test
    void createPayment_shouldThrowUnsupportedExternalResponseException_whenResponseUuidIsNull() {
        PaymentResponseDto response = new PaymentResponseDto();
        response.setUuid(null);
        Passenger passenger = new Passenger("qwe", "asd", "zxc");
        Mockito.when(restTemplate.postForEntity(anyString(), any(PaymentRequestDto.class), any()))
            .thenReturn(new ResponseEntity(response, HttpStatus.OK));
        Assertions.assertThrows(UnsupportedExternalResponseException.class, () ->
            paymentRestClient.createPayment(passenger, 100));
        Mockito.verify(restTemplate, Mockito.times(1)).
            postForEntity(anyString(), any(PaymentRequestDto.class), any());
    }

    @Test
    void createPayment_shouldThrowUnsupportedExternalResponseException_whenResponseUuidIsEmpty() {
        PaymentResponseDto response = new PaymentResponseDto();
        response.setUuid("");
        Passenger passenger = new Passenger("qwe", "asd", "zxc");
        Mockito.when(restTemplate.postForEntity(anyString(), any(PaymentRequestDto.class), any()))
            .thenReturn(new ResponseEntity(response, HttpStatus.OK));
        Assertions.assertThrows(UnsupportedExternalResponseException.class, () ->
            paymentRestClient.createPayment(passenger, 100));
        Mockito.verify(restTemplate, Mockito.times(1)).
            postForEntity(anyString(), any(PaymentRequestDto.class), any());
    }

    @Test
    void createPayment_shouldThrowValidationException_whenNameIsEmpty() {
        Passenger passenger = new Passenger("", "asd", "zxc");
        Assertions.assertThrows(ConstraintViolationException.class, () ->
            paymentRestClient.createPayment(passenger, 100));
        Mockito.verify(restTemplate, Mockito.times(0)).
            postForEntity(anyString(), any(PaymentRequestDto.class), any());
    }

    @Test
    void createPayment_shouldThrowValidationException_whenSurnameIsNull() {
        Passenger passenger = new Passenger("qwe", null, "zxc");
        Assertions.assertThrows(ConstraintViolationException.class, () ->
            paymentRestClient.createPayment(passenger, 100));
        Mockito.verify(restTemplate, Mockito.times(0)).
            postForEntity(anyString(), any(PaymentRequestDto.class), any());
    }

    @Test
    void createPayment_shouldReturnUuid_whenResponseIsCorrect() {
        PaymentResponseDto response = new PaymentResponseDto();
        response.setUuid("123");
        Passenger passenger = new Passenger("qwe", "asd", "zxc");
        Mockito.when(restTemplate.postForEntity(anyString(), any(PaymentRequestDto.class), any()))
            .thenReturn(new ResponseEntity(response, HttpStatus.OK));
        Assertions.assertEquals("123", paymentRestClient.createPayment(passenger, 100));
        Mockito.verify(restTemplate, Mockito.times(1)).
            postForEntity(anyString(), any(PaymentRequestDto.class), any());
    }

    @Test
    void checkPaymentStatus_shouldThrowRestClientException_whenServerReturnError() {
        String paymentUUID = "qwe123";
        String requestUrl = testURL + paymentUUID + "/status";
        Mockito.when(restTemplate.exchange(requestUrl, HttpMethod.GET, null,
            new ParameterizedTypeReference<PaymentCheckResponseDto>() {}))
            .thenThrow(RestClientException.class);
        Assertions.assertThrows(RestClientException.class, () ->paymentRestClient.checkPaymentStatus(paymentUUID));
        Mockito.verify(restTemplate, Mockito.times(1))
            .exchange(anyString(), any(HttpMethod.class), any(), any(ParameterizedTypeReference.class));
    }

    @Test
    void checkPaymentStatus_shouldThrowUnsupportedExternalResponseException_whenResponseIsEmpty() {
        String paymentUUID = "qwe123";
        String requestUrl = testURL + paymentUUID + "/status";
        Mockito.when(restTemplate.exchange(requestUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<PaymentCheckResponseDto>() {}))
            .thenReturn(new ResponseEntity(HttpStatus.OK));
        Assertions.assertThrows(UnsupportedExternalResponseException.class, () ->
            paymentRestClient.checkPaymentStatus(paymentUUID));
        Mockito.verify(restTemplate, Mockito.times(1))
            .exchange(anyString(), any(HttpMethod.class), any(), any(ParameterizedTypeReference.class));
    }

    @Test
    void checkPaymentStatus_shouldThrowUnsupportedExternalResponseException_whenResponseStatusIsNull() {
        String paymentUUID = "qwe123";
        String requestUrl = testURL + paymentUUID + "/status";
        PaymentCheckResponseDto response = new PaymentCheckResponseDto();
        response.setPaymentStatus(null);
        Mockito.when(restTemplate.exchange(requestUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<PaymentCheckResponseDto>() {}))
            .thenReturn(new ResponseEntity(response, HttpStatus.OK));
        Assertions.assertThrows(UnsupportedExternalResponseException.class, () ->
            paymentRestClient.checkPaymentStatus(paymentUUID));
        Mockito.verify(restTemplate, Mockito.times(1))
            .exchange(anyString(), any(HttpMethod.class), any(), any(ParameterizedTypeReference.class));
    }

    @Test
    void checkPaymentStatus_shouldReturnPaymentStatus_whenResponseStatusIsValid() {
        String paymentUUID = "qwe123";
        String requestUrl = testURL + paymentUUID + "/status";
        PaymentCheckResponseDto response = new PaymentCheckResponseDto();
        response.setPaymentStatus(PaymentStatus.DONE);
        Mockito.when(restTemplate.exchange(requestUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<PaymentCheckResponseDto>() {}))
            .thenReturn(new ResponseEntity(response, HttpStatus.OK));
        Assertions.assertEquals(PaymentStatus.DONE, paymentRestClient.checkPaymentStatus(paymentUUID));
        Mockito.verify(restTemplate, Mockito.times(1))
            .exchange(anyString(), any(HttpMethod.class), any(), any(ParameterizedTypeReference.class));
    }

    @Test
    void checkPaymentStatus_shouldThrowIllegalArgumentException_whenInputIsNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            paymentRestClient.checkPaymentStatus(null));
    }
}
