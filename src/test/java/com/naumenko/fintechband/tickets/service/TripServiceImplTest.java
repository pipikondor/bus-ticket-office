package com.naumenko.fintechband.tickets.service;

import com.naumenko.fintechband.tickets.entity.Trip;
import com.naumenko.fintechband.tickets.model.TripViewDto;
import com.naumenko.fintechband.tickets.repository.TicketRepository;
import com.naumenko.fintechband.tickets.repository.TripRepository;
import com.naumenko.fintechband.tickets.service.interfaces.TripService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class TripServiceImplTest {

    @Autowired
    private TripService tripService;

    @MockBean
    private TripRepository tripRepository;

    @MockBean
    private TicketRepository ticketRepository;

    @Test
    void findAllWithAvailablePlaces_shouldReturnTripViewDtoList_whenTripsAreExists() {
        Calendar date = new GregorianCalendar(2020, Calendar.FEBRUARY, 5);
        date.set(Calendar.HOUR, 10);
        date.set(Calendar.MINUTE, 12);
        Trip trip1 = new Trip("London", "New York", date, 12, 20);
        trip1.setId(11);
        Trip trip2 = new Trip("Paris", "Tokyo", date, 120, 60);
        trip2.setId(15);
        TripViewDto tripViewDto1 = new TripViewDto(11, "London", "New York", date, 12, 12);
        TripViewDto tripViewDto2 = new TripViewDto(15, "Paris", "Tokyo", date, 120, 45);
        List<Trip> trips = new ArrayList<>();
        trips.add(trip1);
        trips.add(trip2);
        List<TripViewDto> exceptedResult = new ArrayList<>();
        exceptedResult.add(tripViewDto1);
        exceptedResult.add(tripViewDto2);
        Mockito.when(tripRepository.findAll()).thenReturn(trips);
        Mockito.when(ticketRepository.getTicketCount(11)).thenReturn(8);
        Mockito.when(ticketRepository.getTicketCount(15)).thenReturn(15);
        Assertions.assertEquals(exceptedResult, tripService.findAllWithAvailablePlaces());
    }
}
