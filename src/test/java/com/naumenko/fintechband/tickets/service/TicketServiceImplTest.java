package com.naumenko.fintechband.tickets.service;

import com.naumenko.fintechband.tickets.entity.Passenger;
import com.naumenko.fintechband.tickets.entity.Payment;
import com.naumenko.fintechband.tickets.entity.Ticket;
import com.naumenko.fintechband.tickets.entity.Trip;
import com.naumenko.fintechband.tickets.exceptions.TicketIsNotExistsException;
import com.naumenko.fintechband.tickets.model.PaymentStatus;
import com.naumenko.fintechband.tickets.model.TicketOrderDto;
import com.naumenko.fintechband.tickets.repository.TicketRepository;
import com.naumenko.fintechband.tickets.repository.TripRepository;
import com.naumenko.fintechband.tickets.service.interfaces.TicketService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityNotFoundException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class TicketServiceImplTest {

    @Autowired
    private TicketService ticketService;

    @MockBean
    private TicketRepository ticketRepository;

    @MockBean
    private TripRepository tripRepository;

    @MockBean
    private PaymentRestClient paymentRestClient;

    @Test
    void buyTicket_shouldThrowEntityNotFoundException_whenTripIdIsNotExist() {
        TicketOrderDto order = new TicketOrderDto(11, "asd", "qwe", "tyu");
        Mockito.when(tripRepository.findById(11)).thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class, () -> ticketService.buyTicket(order));
        Mockito.verify(tripRepository, Mockito.times(1)).findById(anyInt());
        Mockito.verify(ticketRepository, Mockito.times(0)).getTicketCount(anyInt());
        Mockito.verify(paymentRestClient, Mockito.times(0)).createPayment(any(Passenger.class), anyInt());
        Mockito.verify(ticketRepository, Mockito.times(0)).save(any(Ticket.class));
    }

    @Test
    void buyTicket_shouldThrowTicketIsNotExistsException_whenFreeTicketIsNotExists() {
        Calendar date = new GregorianCalendar(2020, Calendar.FEBRUARY, 5);
        date.set(Calendar.HOUR, 10);
        date.set(Calendar.MINUTE, 12);
        Trip trip = new Trip("London", "New York", date, 12, 20);
        trip.setId(11);
        TicketOrderDto order = new TicketOrderDto(11, "asd", "qwe", "tyu");
        Mockito.when(tripRepository.findById(11)).thenReturn(Optional.of(trip));
        Mockito.when(ticketRepository.getTicketCount(11)).thenReturn(20);
        Assertions.assertThrows(TicketIsNotExistsException.class, () -> ticketService.buyTicket(order));
        Mockito.verify(tripRepository, Mockito.times(1)).findById(anyInt());
        Mockito.verify(ticketRepository, Mockito.times(1)).getTicketCount(anyInt());
        Mockito.verify(paymentRestClient, Mockito.times(0)).createPayment(any(Passenger.class), anyInt());
        Mockito.verify(ticketRepository, Mockito.times(0)).save(any(Ticket.class));
    }

    @Test
    void buyTicket_shouldReturnTicketWithId_whenTicketIsAvailableAndTripIsExist() {
        Calendar date = new GregorianCalendar(2020, Calendar.FEBRUARY, 5);
        date.set(Calendar.HOUR, 10);
        date.set(Calendar.MINUTE, 12);
        Trip trip = new Trip("London", "New York", date, 12, 20);
        trip.setId(11);
        Passenger passenger = new Passenger("asd", "qwe", "tyu");
        Payment payment = new Payment("123qwe", PaymentStatus.NEW);
        passenger.setId(1);
        Ticket exceptedTicket = new Ticket(trip, passenger, payment);
        exceptedTicket.setTicketId(19);
        Ticket ticket = new Ticket(trip, passenger, payment);
        TicketOrderDto order = new TicketOrderDto(11, "asd", "qwe", "tyu");
        Mockito.when(tripRepository.findById(11)).thenReturn(Optional.of(trip));
        Mockito.when(ticketRepository.getTicketCount(11)).thenReturn(18);
        Mockito.when(paymentRestClient.createPayment(passenger, 12)).thenReturn("123qwe");
        Mockito.when(ticketRepository.save(ticket)).thenReturn(exceptedTicket);
        Assertions.assertEquals(exceptedTicket, ticketService.buyTicket(order));
        Mockito.verify(tripRepository, Mockito.times(1)).findById(anyInt());
        Mockito.verify(ticketRepository, Mockito.times(1)).getTicketCount(anyInt());
        Mockito.verify(paymentRestClient, Mockito.times(1)).createPayment(any(Passenger.class), anyInt());
        Mockito.verify(ticketRepository, Mockito.times(1)).save(any(Ticket.class));
    }
}
